# gpio_test

This program is developed to test the GPIO interface present on the EMP. In this test, at least two GPIO must be connected. One GPIO will act as the output in this configuration, while the other will be the input. The program will now detect if the inputs can detect the signal from the output and vice versa. Based on this result, the GPIO can be determined to be either sufficient or inaccurate. 

# Getting Started

A more in depth walk through is shown in chapter 6 - Development of the GPIO test - in Emil's documentation.

1. Design VHDL module with the desired AXI GPIO(s) created
2. Generate the hardware on the EMP
3. Compile the code using the command: gcc gpio.c -o gpio

# Usage

Run the program followed by the base number of the gpiochip. E.g. for gpiochip328 it would be:
$ ./gpip_test 328
